import java.util.ArrayList;
import java.util.Arrays;

public class Board {
    private ArrayList<ArrayList<String>> board = new ArrayList<>();
    //private ArrayList<ArrayList<Integer>> behindMatrix = new ArrayList<>();

    public Board() {
        createBoard();
    }


    private void createBoard() {
        int rows = 5;
        int columns = 11;
//        int[] emptyCol = new int[]{0, 2, 4, 6, 8, 10};
//        int[] underscore = new int[]{1, 5, 9};
//        int[] straightLine = new int[]{3, 7};
        for (int row = 0; row < rows; row++) {
            board.add(new ArrayList<>());
            for (int col = 0; col < columns; col++) {
                if (row == 1 || row == 3) {
                    board.get(row).add("-");
                    //System.out.print("-");
                } else {
                    if (col == 0 || col == 2 || col == 4 || col == 6 || col == 8 || col == 10) {
                        board.get(row).add(" ");
                        //System.out.print(" ");
                    } else if (col == 1 || col == 5 || col == 9) {
                        board.get(row).add("_");
                        //System.out.print("_");
                    } else if (col == 3 || col == 7) {
                        board.get(row).add("|");
                        //System.out.print("|");
                    }
                }
            }
        }
    }

//    private void createBehindMatrix(){
//        for(int row = 0; row < 3; row ++){
//            behindMatrix.add(new ArrayList<>());
//            for (int col = 0; col < 3; col++){
//                behindMatrix.get(row).add(0);
//            }
//        }
//    }

    public void displayBoard() {
        for (int row = 0; row < board.size(); row++) {
            for (int col = 0; col < board.get(row).size(); col++) {
                System.out.print(board.get(row).get(col));
            }
            System.out.println();
        }
    }

    private void convertSpot(int rowNumber, int colNumber, String value) {
        board.get(rowNumber).set(colNumber, value);
    }

    public void changeSpot(int rowNumber, int colNumber, String value) {
        int row = 0;
        int col = 0;
        switch (rowNumber) {
            case 0:
                row = 0;
                break;
            case 1:
                row = 2;
                break;
            case 2:
                row = 4;
                break;
        }

        switch (colNumber) {
            case 0:
                col = 1;
                break;
            case 1:
                col = 5;
                break;
            case 2:
                col = 9;
                break;
        }
        convertSpot(row, col, value);
    }

    public String getASpot(int rowNumber, int colNumber) {
        int row = 0;
        int col = 0;
        switch (rowNumber) {
            case 0:
                row = 0;
                break;
            case 1:
                row = 2;
                break;
            case 2:
                row = 4;
                break;
        }

        switch (colNumber) {
            case 0:
                col = 1;
                break;
            case 1:
                col = 5;
                break;
            case 2:
                col = 9;
                break;
        }
        return board.get(row).get(col);
    }

    public boolean isSpotEmpty(int row, int col) {
        if (getASpot(row, col) == "_") {
            return true;
        } else {
            return false;
        }
    }

    public boolean isAWinner(String symbol) {
        int rowLength = 3;
        int colLength = 3;
        boolean bool = false;
        int colCounter = 0;
        int rowCounter = 0;
        //Search through columns
        for (int row = 0; row < rowLength; row++) {
            colCounter = 0;
            for (int col = 0; col < colLength; col++) {
                if (getASpot(row, col) == symbol) {
                    colCounter++;
                }
            }
            if (colCounter == 3) {
                bool = true;
                break;
            }
        }
        //Search through rows
        for (int col = 0; col < colLength; col++) {
            rowCounter = 0;
            for (int row = 0; row < rowLength; row++) {
                if (getASpot(row, col) == symbol) {
                    rowCounter++;
                }
            }
            if (rowCounter == 3) {
                bool = true;
                break;
            }
        }

        int counterBackSlash = 0;
        int counterSlash = 0;
        //Search through diagonals
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                if (row - col == 0) {
                    if (getASpot(row, col).equals(symbol)) {
                        counterBackSlash++;
                    }
                }
                if (row + col == rowLength - 1) {
                    if (getASpot(row, col).equals(symbol)) {
                        counterSlash++;
                    }
                }
            }
        }
        if (counterBackSlash == 3 || counterSlash == 3) {
            bool = true;
        }

        return bool;
    }

    public void resetBoard() {
        int rowLenght = 3;
        int colLenght = 3;
        for (int row = 0; row < rowLenght; row++) {
            for (int col = 0; col < colLenght; col++) {
                this.changeSpot(row, col, "_");
            }
        }
    }


}
