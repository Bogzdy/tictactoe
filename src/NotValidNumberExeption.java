
public class NotValidNumberExeption extends Exception {
    private int minim;
    private int maxim;

    public NotValidNumberExeption(int minim, int maxim) {
        this.minim = minim;
        this.maxim = maxim;
    }

    public int getMinim() {
        return minim;
    }

    public int getMaxim() {
        return maxim;
    }
}
