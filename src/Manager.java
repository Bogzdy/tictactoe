import javax.sql.rowset.serial.SerialArray;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {
    Scanner scanner = new Scanner(System.in);
    private Player currentPlayer;
    private Player lastPlayer;
    private Player playerOne;
    private Player playerTwo;
    private Board board = new Board();

    //Player vs Player
    public void startGamePvP() {
        System.out.println("Insert first player name (X): ");
        String firstPlayer = scanner.nextLine();
        System.out.println("Insert second player name (0): ");
        String secondPlayer = scanner.nextLine();
        playerOne = new Player(firstPlayer);
        playerOne.setSymbol("X");
        playerTwo = new Player(secondPlayer);
        playerTwo.setSymbol("0");
        randomPlayerToStart();

        int moves = 0;
        boolean isWinner = false;
        while (!isWinner) {
            moves++;
            board.displayBoard();
            printWhosNext();
            String coord = getInput();
            boolean isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
            isWinner = isWinner(currentPlayer);
            while (!isSymbolP) {
                printWhosNext();
                coord = getInput();
                isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
                isWinner = isWinner(currentPlayer);
            }
            switchPlayer();
            if (moves == 9 && !isWinner) {
                board.displayBoard();
                System.out.println("Draw!");
                isWinner = true;
            }
        }
    }

    // Player vs bot (easy)
    public void startGameWithBotEasy() {
        System.out.println("Insert first player name (X): ");
        String firstPlayer = scanner.nextLine();
        playerOne = new Player(firstPlayer);
        playerOne.setSymbol("X");
        playerTwo = new Robot();
        playerTwo.setSymbol("0");
        randomPlayerToStart();
        int moves = 0;

        boolean isWinner = false;
        while (!isWinner) {
            moves++;
            board.displayBoard();
            printWhosNext();
            String coord = "";
            if (currentPlayer instanceof Robot) {
                ((Robot) currentPlayer).printSpotsToWin(((Robot) currentPlayer).goForAWin(board, currentPlayer.getSymbol()));
                ((Robot) currentPlayer).printSpotsToLose(((Robot) currentPlayer).blockALine(board, playerOne.getSymbol()));
                coord = ((Robot) currentPlayer).generateCoordinate();
            } else {
                coord = getInput();
            }

            boolean isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
            isWinner = isWinner(currentPlayer);
            while (!isSymbolP) {
                printWhosNext();
                if (currentPlayer instanceof Robot) {
                    coord = ((Robot) currentPlayer).generateCoordinate();
                } else {
                    coord = getInput();
                }
                isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
                isWinner = isWinner(currentPlayer);
            }
            switchPlayer();
            if (moves == 9 && !isWinner) {
                System.out.println("Draw!");
                isWinner = true;
            }
        }
    }

    //Player vs bot hard
    public void startGameWithBotHard(boolean isAHuman) {
        board.resetBoard();
        if (isAHuman) {
            System.out.println("Insert first player name (X): ");
            String firstPlayer = scanner.nextLine();
            playerOne = new Player(firstPlayer);
            playerOne.setSymbol("X");
            playerTwo = new HardBot();
            playerTwo.setSymbol("0");
            randomPlayerToStart();
        } else {
            playerOne = new HardBot("HardBotTwo");
            playerOne.setSymbol("X");
            playerTwo = new HardBot();
            playerTwo.setSymbol("0");
            randomPlayerToStart();
        }
        int moves = 0;
        boolean isWinner = false;
        while (!isWinner) {
            moves++;
            board.displayBoard();
            printWhosNext();
            String coord = "";
            if (currentPlayer instanceof HardBot) {
                ArrayList<String> arrayOfBadSpots = ((HardBot) currentPlayer).blockALine(board, lastPlayer.getSymbol());
                ArrayList<String> arrayOfGoodSpots = ((HardBot) currentPlayer).goForAWin(board, currentPlayer.getSymbol());

                if (moves == 1) {
                    coord = ((HardBot) currentPlayer).chooseAnCorner(board);
                } else if (moves == 2) {
                    coord = ((HardBot) currentPlayer).chooseTheMiddle();
                } else if (moves == 3) {
                    coord = ((HardBot) currentPlayer).chooseOppossiteCorner(((HardBot) currentPlayer).getFirstCorner());
                } else if (moves == 4) {
                    if (arrayOfBadSpots.isEmpty()) {
                        coord = ((HardBot) currentPlayer).chooseASide(board);
                    } else {
                        coord = arrayOfBadSpots.get(0);
                    }
                } else if (moves > 4) {
                    if (!arrayOfGoodSpots.isEmpty()) {
                        coord = arrayOfGoodSpots.get(0);
                    } else if (!arrayOfBadSpots.isEmpty()) {
                        coord = arrayOfBadSpots.get(0);
                    } else {
                        coord = ((HardBot) currentPlayer).generateCoordinate();
                    }
                }
                arrayOfBadSpots.clear();
                arrayOfGoodSpots.clear();
            } else {
                coord = getInput();
            }

            boolean isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
            isWinner = isWinner(currentPlayer);
            while (!isSymbolP) {
                printWhosNext();
                if (currentPlayer instanceof HardBot) {
                    coord = ((HardBot) currentPlayer).generateCoordinate();
                } else {
                    coord = getInput();
                }
                isSymbolP = isSymbolPlaced(getCoordinates(coord, 0), getCoordinates(coord, 1), currentPlayer);
                isWinner = isWinner(currentPlayer);
            }
            switchPlayer();
            if (moves == 9 && !isWinner) {
                System.out.println("Draw!");
                board.displayBoard();
                isWinner = true;
            }
        }

    }

    public void switchPlayer() {
        if (this.currentPlayer == this.playerTwo) {
            this.currentPlayer = this.playerOne;
            this.lastPlayer = playerTwo;
        } else {
            this.currentPlayer = this.playerTwo;
            this.lastPlayer = playerOne;
        }
    }

    private void randomPlayerToStart() {
        int random = (int) Math.round(Math.random());
        switch (random) {
            case 0:
                currentPlayer = playerOne;
                lastPlayer = playerTwo;
                break;
            case 1:
                currentPlayer = playerTwo;
                lastPlayer = playerOne;
                break;
        }
    }

    private void printWhosNext() {
        System.out.println(currentPlayer.getName() + "(" + currentPlayer.getSymbol() + ") " + ", enter your move (eg. \"1,1\") (row[1-3] column[1-3]): ");
    }

    private String getInput() {
        String input = scanner.nextLine();
        while (!input.matches("(\\d{1},)?(\\d{1})")){
            System.out.println("Try again (eg. 1,1):");
            input = scanner.nextLine();
        }
        String[] coord = input.split(",");
        int row = Integer.parseInt(coord[0]) - 1;
        int col = Integer.parseInt(coord[1]) - 1;
        input = (row + "") + "," + (col + "");
        return input;
    }

    private int getCoordinates(String input, int index) {
        String[] coordinates = input.split(",");
        return Integer.parseInt(coordinates[index]);
    }

    public boolean isSymbolPlaced(int row, int col, Player player) {
        if (board.isSpotEmpty(row, col)) {
            board.changeSpot(row, col, player.getSymbol());
            return true;
        } else {
            System.out.println("This move at (" + (row + 1) + ", " + (col + 1) + ") is not valid. Try again...");
            return false;
        }
    }

    public boolean isWinner(Player player) {
        boolean bool = board.isAWinner(player.getSymbol());
        if (bool) {
            System.out.println(player.getName() + " IS THE WINNER!");
            board.displayBoard();
        }
        return bool;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }
}
