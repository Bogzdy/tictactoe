import java.util.ArrayList;

public class Robot extends Player {
    private String symbol;

    public Robot() {
        super("Bot");
    }

    public String generateCoordinate() {
        int randomNumRow = (int) (Math.random() * 3);
        String randomRow = "" + randomNumRow;
        int randomNumCol = (int) (Math.random() * 3);
        String randmCol = "" + randomNumCol;
        String returnedString = randomRow + "," + randmCol;
        return returnedString;
    }

    public ArrayList<String> blockALine(Board board, String playerSymbol) {
        ArrayList<String> spotsToLose = new ArrayList<>();

        int rowLength = 3;
        int colLength = 3;
        int counterSymbol = 0;
        String keepIt = "";
        //Search through columns
        for (int row = 0; row < rowLength; row++) {
            counterSymbol = 0;
            for (int col = 0; col < colLength; col++) {
                if (board.getASpot(row, col).equals(playerSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }
                }
            }
            if (counterSymbol == colLength - 1) {
                spotsToLose.add(keepIt);
            }
        }
        //Search through rows
        for (int col = 0; col < colLength; col++) {
            counterSymbol = 0;
            for (int row = 0; row < rowLength; row++) {
                if (board.getASpot(row, col).equals(playerSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }

                }
            }
            if (counterSymbol == rowLength - 1) {
                spotsToLose.add(keepIt);
            }
        }

        //Search through diagonals
        int counterSymbolBackSlash = 0;
        int counterSymbolSlash = 0;
        String keepItForBackSlash = "";
        String keepItForSlash = "";
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                //BackSlash
                if (row - col == 0) {
                    if (board.getASpot(row, col).equals(playerSymbol)) {
                        counterSymbolBackSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForBackSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
                //Slash
                if (row + col == rowLength - 1) {
                    if (board.getASpot(row, col).equals(playerSymbol)) {
                        counterSymbolSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
            }
        }

        if (counterSymbolBackSlash == rowLength - 1) {
            spotsToLose.add(keepItForBackSlash);
        }

        if (counterSymbolSlash == rowLength - 1) {
            spotsToLose.add(keepItForSlash);
        }

        return spotsToLose;
    }

    public ArrayList<String> goForAWin(Board board, String botSymbol) {
        ArrayList<String> spotsToWin = new ArrayList<>();

        int rowLength = 3;
        int colLength = 3;
        int counterSymbol = 0;
        String keepIt = "";
        for (int row = 0; row < rowLength; row++) {
            counterSymbol = 0;
            for (int col = 0; col < colLength; col++) {
                if (board.getASpot(row, col).equals(botSymbol)) {
                    counterSymbol++;
                } else {
                    keepIt = (row + "") + "," + (col + "");
                }
            }
            if (counterSymbol == colLength - 1) {
                spotsToWin.add(keepIt);
            }
        }

        for (int col = 0; col < colLength; col++) {
            counterSymbol = 0;
            for (int row = 0; row < rowLength; row++) {
                if (board.getASpot(row, col).equals(botSymbol)) {
                    counterSymbol++;
                } else {
                    keepIt = (row + "") + "," + (col + "");

                }
            }
            if (counterSymbol == rowLength - 1) {
                spotsToWin.add(keepIt);
            }
        }

        //Search through diagonals
        int counterSymbolBackSlash = 0;
        int counterSymbolSlash = 0;
        String keepItForBackSlash = "";
        String keepItForSlash = "";
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                //BackSlash
                if (row - col == 0) {
                    if (board.getASpot(row, col).equals(botSymbol)) {
                        counterSymbolBackSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForBackSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
                //Slash
                if (row + col == rowLength - 1) {
                    if (board.getASpot(row, col).equals(botSymbol)) {
                        counterSymbolSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
            }
        }

        if (counterSymbolBackSlash == rowLength - 1) {
            spotsToWin.add(keepItForBackSlash);
        }

        if (counterSymbolSlash == rowLength - 1) {
            spotsToWin.add(keepItForSlash);
        }
        return spotsToWin;
    }

    public void printSpotsToLose(ArrayList<String> array) {
        System.out.print("Spots to lose = ");
        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + ", ");
        }
        System.out.println();
    }

    public void printSpotsToWin(ArrayList<String> array) {
        System.out.print("Spots to win = ");
        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + ", ");
        }
        System.out.println();
    }
}
