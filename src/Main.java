import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Manager manager = new Manager();
        System.out.println("1. Player vs player.");
        System.out.println("2. Player vs  bot(easy).");
        System.out.println("3. Player vs bot(hard).");
        System.out.println("4. Bot(hard) vs bot(hard).");
        System.out.print("What's your choice: ");
        int choice = 0;
        boolean isNumber = false;
        while (!isNumber) {

            try {
                choice = scanner.nextInt();
                scanner.nextLine();
                if (choice <= 0 || choice > 4) {
                    throw new NotValidNumberExeption(1, 4);
                } else {
                    isNumber = true;
                }

                isNumber = true;
            } catch (java.util.InputMismatchException e) {
                System.out.println("You have to select a number!");
                scanner.nextLine();

            } catch (NotValidNumberExeption f) {
                System.out.println("Choose a number between " + f.getMinim() + " and " + f.getMaxim());

            }
        }


        switch (choice) {
            case 1:
                manager.startGamePvP();
                break;
            case 2:
                manager.startGameWithBotEasy();
                break;
            case 3:
                manager.startGameWithBotHard(true);
                break;
            case 4:
                System.out.print("How many rounds? ");
                int rounds = 0;
                boolean isAValidNumber = false;
                while (!isAValidNumber) {
                    try {
                        rounds = scanner.nextInt();
                        scanner.nextLine();
                        isAValidNumber = true;
                    } catch (java.util.InputMismatchException e) {
                        System.out.print("You have to select a number!");
                        System.out.println(" How many rounds? ");
                        scanner.nextLine();
                    }
                }

                int playerOneWins = 0;
                int playerTwoWins = 0;
                int ties = 0;
                while (rounds != 0) {
                    System.out.println("------------GAME STARTED--------------");
                    manager.startGameWithBotHard(false);
                    if (manager.isWinner(manager.getPlayerOne())) {
                        playerOneWins++;
                    } else if (manager.isWinner(manager.getPlayerTwo())) {
                        playerTwoWins++;
                    } else {
                        ties++;
                    }
                    rounds--;
                }
                System.out.println("HardBotOne " + playerOneWins + " wins.");
                System.out.println("HardBotTwo " + playerTwoWins + " wins.");
                System.out.println(ties + " ties.");
                break;
        }
    }
}
