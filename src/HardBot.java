import java.util.ArrayList;

public class HardBot extends Player {
    private ArrayList<String> spotsToLose = new ArrayList<>();
    private ArrayList<String> spotsToWin = new ArrayList<>();
    private String symbol;
    private int myMoves = 0;
    private String firstCorner;


    public HardBot() {
        super("HardBotTwo");
    }

    public HardBot(String name) {
        super(name);
    }

    public String getFirstCorner() {
        return firstCorner;
    }

    public String generateCoordinate() {
        int randomNumRow = (int) (Math.random() * 3);
        String randomRow = "" + randomNumRow;
        int randomNumCol = (int) (Math.random() * 3);
        String randmCol = "" + randomNumCol;
        String returnedString = randomRow + "," + randmCol;
        return returnedString;
    }

    public String chooseAnCorner(Board board) {
        ArrayList<String> angles = new ArrayList<>();
        int rowLength = 3;
        int colLength = 3;
        String angle = "";

        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                if ((row == 0 || row == rowLength - 1) && (col == 0 || col == colLength - 1) && board.getASpot(row, col).equals("_")) {
                    angle = (row + "") + "," + (col + "");
                    angles.add(angle);
                }
            }
        }
        int randomAngle = (int) (Math.random() * angles.size());
        firstCorner = angles.get(randomAngle);
        return firstCorner;
    }

    public String chooseTheMiddle() {
        return "1,1";
    }

    public String chooseASide(Board board) {
        ArrayList<String> sides = new ArrayList<>();
        int rowLength = 3;
        int colLength = 3;
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                if (row == 0 || row == rowLength - 1) {
                    if (col > 0 && col < colLength - 1) {
                        sides.add((row + "") + "," + (col + ""));
                    } else if (row > 0 && row < rowLength - 1) {
                        if (col == 0 || col == colLength - 1) {
                            sides.add((row + "") + "," + (col + ""));
                        }
                    }
                }
            }
        }
        int randomSide = (int) (Math.random() * sides.size());
        return sides.get(randomSide);
    }

    public String chooseOppossiteCorner(String firstCorner) {
        String oppositeCorner = "";
        switch (firstCorner) {
            case "0,0":
                oppositeCorner = "2,2";
                break;
            case "2,2":
                oppositeCorner = "0,0";
                break;
            case "0,2":
                oppositeCorner = "2,0";
                break;
            case "2,0":
                oppositeCorner = "0,2";
                break;
        }
        return oppositeCorner;
    }


    public ArrayList<String> blockALine(Board board, String playerSymbol) {
        final int rowLength = 3;
        final int colLength = 3;
        int counterSymbol = 0;
        String keepIt = "";
        //Search through columns
        for (int row = 0; row < rowLength; row++) {
            counterSymbol = 0;
            keepIt = "";
            for (int col = 0; col < colLength; col++) {
                if (board.getASpot(row, col).equals(playerSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }
                }
            }
            if (counterSymbol == colLength - 1 && !keepIt.equals("")) {
                spotsToLose.add(keepIt);
            }
        }
        //Search through rows

        for (int col = 0; col < colLength; col++) {
            counterSymbol = 0;
            keepIt = "";
            for (int row = 0; row < rowLength; row++) {
                if (board.getASpot(row, col).equals(playerSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }
                }
            }
            if (counterSymbol == rowLength - 1 && !keepIt.equals("")) {
                spotsToLose.add(keepIt);
            }
        }

        //Search through diagonals
        int counterSymbolBackSlash = 0;
        int counterSymbolSlash = 0;
        String keepItForBackSlash = "";
        String keepItForSlash = "";
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                //BackSlash
                if (row - col == 0) {
                    if (board.getASpot(row, col).equals(playerSymbol)) {
                        counterSymbolBackSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForBackSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
                //Slash
                if (row + col == rowLength - 1) {
                    if (board.getASpot(row, col).equals(playerSymbol)) {
                        counterSymbolSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
            }
        }

        if (counterSymbolBackSlash == rowLength - 1 && !keepItForBackSlash.equals("")) {
            spotsToLose.add(keepItForBackSlash);
        }

        if (counterSymbolSlash == rowLength - 1 && !keepItForSlash.equals("")) {
            spotsToLose.add(keepItForSlash);
        }

        return spotsToLose;
    }

    public ArrayList<String> goForAWin(Board board, String botSymbol) {
        int rowLength = 3;
        int colLength = 3;
        int counterSymbol = 0;
        String keepIt = "";
        //Search through columns
        for (int row = 0; row < rowLength; row++) {
            counterSymbol = 0;
            keepIt = "";
            for (int col = 0; col < colLength; col++) {
                if (board.getASpot(row, col).equals(botSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }
                }
            }
            if (counterSymbol == colLength - 1 && !keepIt.equals("")) {
                spotsToWin.add(keepIt);
            }
        }
        //Search through rows

        for (int col = 0; col < colLength; col++) {
            counterSymbol = 0;
            keepIt = "";
            for (int row = 0; row < rowLength; row++) {
                if (board.getASpot(row, col).equals(botSymbol)) {
                    counterSymbol++;
                } else {
                    if (board.getASpot(row, col).equals("_")) {
                        keepIt = (row + "") + "," + (col + "");
                    }
                }
            }
            if (counterSymbol == rowLength - 1 && !keepIt.equals("")) {
                spotsToWin.add(keepIt);
            }
        }

        //Search through diagonals
        int counterSymbolBackSlash = 0;
        int counterSymbolSlash = 0;
        String keepItForBackSlash = "";
        String keepItForSlash = "";
        for (int row = 0; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                //BackSlash
                if (row - col == 0) {
                    if (board.getASpot(row, col).equals(botSymbol)) {
                        counterSymbolBackSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForBackSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
                //Slash
                if (row + col == rowLength - 1) {
                    if (board.getASpot(row, col).equals(botSymbol)) {
                        counterSymbolSlash++;
                    } else {
                        if (board.getASpot(row, col).equals("_")) {
                            keepItForSlash = (row + "") + "," + (col + "");
                        }
                    }
                }
            }
        }

        if (counterSymbolBackSlash == rowLength - 1 && !keepItForBackSlash.equals("")) {
            spotsToWin.add(keepItForBackSlash);
        }

        if (counterSymbolSlash == rowLength - 1 && !keepItForSlash.equals("")) {
            spotsToWin.add(keepItForSlash);
        }
        return spotsToWin;
    }

    public void printSpotsToLose(ArrayList<String> array) {
        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + ", ");
        }
        System.out.println();
    }

    public void printSpotsToWin(ArrayList<String> array) {
        for (int i = 0; i < array.size(); i++) {
            System.out.print(array.get(i) + ", ");
        }
        System.out.println();
    }

    public void clearArraysLoseWin() {
        spotsToLose.clear();
        spotsToWin.clear();
    }
}
